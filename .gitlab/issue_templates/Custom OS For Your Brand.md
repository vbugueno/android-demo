## BOOT Animation
##### (If you want to display your brand boot animation,please convert the boot animation to 480*800 BMP format picture and share us)


## Preinstall Apps
##### (If you want to preinstall your app,please share us your app)


## Wallpaper
##### (If you want to display your brand wallpaper,please share us a 480*800 BMP/PNG/JPG format picture)


## Language 
##### (Please tell me your favorite language,English,Spanish,Chinese,etc)
- Keyboard (Please tell me your favorite keyboard type)


## APN list
##### (If you want to add new APN, please follow the below template to provide us new APN parameters)

- apn carrier="SKT LTE INTERNET"
- mcc="450"
- mnc="05"
- apn="lte.sktelecom.com"
- mmsc="http://omms.nate.com:9082/oma_mms"
- mmsproxy="lteoma.nate.com"
- mmsport="9093"
- type="default,supl,mms,dun"
- protocol="IPV4V6"
- roaming_protocol="IPV4V6"
- network_type_bitmask="13|19"


## Launcher App
##### (If you want to set your app as launcher app, please share me your app)

## App Install Signature Restrictions
##### (If you want to restrict customers from installing other apps to keep the device safe, please share me your app，we will get signature from your app and inject into OS)